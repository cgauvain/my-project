import React from 'react';

class Clock extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      time: new Date().toLocaleString()
    };
  }
  componentDidMount () {
    this.IntervalID = setInterval(
      () => this.updateClock(),
      1000
    );
  }
  componentWillUnmount () {
    clearInterval(this.IntervalID);
  }
  updateClock () {
    this.setState({
      time: new Date().toLocaleString()
    });
  }
  render () {
    return (
      <p>
        The time is {this.state.time};
      </p>
    );
  }
}

export default Clock;
