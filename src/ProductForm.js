import React from 'react';

var RESET_VALUES = {
  name: '',
  category: '',
  price: '',
  inStock: false
};

class ProductForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
    this.validateFields = this.validateFields.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.state = {
      product: Object.assign({}, RESET_VALUES),
      errors: {}
    };
  }
  handleChange(e) {
    const target = e.target;
    const name = target.name;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    this.setState((prevState) => {
      prevState.product[name] = value;
      return { product: prevState.product }
    });
  }
  validateFields() {
    let isValid = true;
    let errors = {};
    if (!this.state.product.name) {
      errors['name'] = "Name is required";
      isValid = false;
    }
    if (!isValid) {
      this.setState((prevState) => {
        prevState.errors = errors;
        return prevState;
      });
    }
    return isValid;
  }
  handleSave(e) {
    if (this.validateFields()) {
      this.props.onSave(this.state.product)
      // reset the form values to blank after submitting:
      this.setState({
        product: Object.assign({}, RESET_VALUES),
        errors: {}
      });
    }
    // prevent the form submit event from triggering an HTTP Post:
    e.preventDefault();
  }
  render () {
    let nameError = null;
    if ("name" in this.state.errors) {
      nameError = this.state.errors["name"];
    }
    return (
      <form>
        <h3>Enter a new product</h3>
        <p>
          <label>
            Name
            <br />
            <input
              type="text"
              name="name"
              onChange={this.handleChange}
              value={this.state.product.name}
            />
          </label>
          {nameError}
        </p>
        <p>
          <label>
            Category
            <br />
            <input
              type="text"
              name="category"
              onChange={this.handleChange}
              value={this.state.product.category}
            />
          </label>
        </p>
        <p>
          <label>
            Price
            <br />
            <input
              type="text"
              name="price"
              onChange={this.handleChange}
              value={this.state.product.price}
            />
          </label>
        </p>
        <p>
          <label>
            <input
              type="checkbox"
              name="stocked"
              onChange={this.handleChange}
              value={this.state.product.stocked}
            />
            &nbsp;In stock?
          </label>
        </p>
        <input
          type="submit"
          value="Save"
          onClick={this.handleSave}
        />
      </form>
    );
  }
}

export default ProductForm;
