import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Products from './Products.js';

class App extends Component {
  render() {
    return (
      <div>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Products list</h1>
        </header>
        <section>
          <Products />
        </section>
      </div>
    );
  }
}

export default App;
