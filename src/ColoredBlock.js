import React from 'react';
import ChangeColorButton from './ChangeColorButton.js'

class ColoredBlock extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      backgroundColor: "red"
    }
    this.changeColor = this.changeColor.bind(this);
  }
  changeColor () {
    let newColor = this.state.backgroundColor === "red" ? "blue" : "red";
    this.setState((prevState) => {
      return {
        backgroundColor: newColor
      }
    });
  }
  render () {
    return (
      <div style={{width: "200px", height: "200px", "backgroundColor": this.state.backgroundColor}}>
        <ChangeColorButton onClick={this.changeColor} currentColor={this.state.backgroundColor}/>
      </div>
    );
  }
}

export default ColoredBlock;
